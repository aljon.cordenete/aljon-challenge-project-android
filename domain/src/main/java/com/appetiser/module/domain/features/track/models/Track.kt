package com.appetiser.module.domain.features.track.models

data class Track (
    val trackId: Long,
    val trackName: String,
    val shortDescription: String?,
    val longDescription: String?,
    val genre: String,
    val price: Float,
    val artwork: String,
    val previewUrl: String,
    val duration: Int
)