package com.appetiser.module.data.features.track

import com.appetiser.module.domain.features.track.models.Track
import com.appetiser.module.local.features.track.TrackLocalSource
import com.appetiser.module.network.features.track.TrackRemoteSource
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers.anyString
import org.mockito.Mockito.*

class TrackRepositoryImplTest {

    //class under test...
    private lateinit var trackRepository: TrackRepository

    private val remoteSource = mock(TrackRemoteSource::class.java)

    private val localSource = mock(TrackLocalSource::class.java)

    @Before
    fun setUp() {
        trackRepository = TrackRepositoryImpl(remoteSource, localSource)
    }

    @Test
    fun `when update tracks is successful, should expect complete`() {
        val expected = arrayListOf<Track>()
            .apply {
                add(Track(1, "track1", "shortDesc", "longDesc", "pop", 1.0f, "", "", 200))
                add(Track(2, "track2", "shortDesc", "longDesc", "pop", 1.0f, "", "", 200))
            }.toList()

        `when`(localSource.saveTracks(expected))
            .thenReturn(listOf())

        `when`(remoteSource.search(anyString(), anyString(), anyString()))
            .thenReturn(Single.just(expected))

        trackRepository.updateTracks("", "", "")
            .test()
            .assertComplete()
            .dispose()

        verify(localSource).saveTracks(expected)
        verify(remoteSource).search(anyString(), anyString(), anyString())
    }

    @Test
    fun `when update tracks failed, should expect error`() {
        val error = Throwable("Something went wrong")

        `when`(remoteSource.search(anyString(), anyString(), anyString()))
            .thenReturn(Single.error(error))

        trackRepository.updateTracks("", "", "")
            .test()
            .assertError(error)
            .dispose()

        verify(remoteSource).search(anyString(), anyString(), anyString())
    }

    @Test
    fun `when loading tracks is successful, should return list of Track`() {
        val expected = arrayListOf<Track>()
            .apply {
                add(Track(1, "track1", "shortDesc", "longDesc", "pop", 1.0f, "", "", 200))
                add(Track(2, "track2", "shortDesc", "longDesc", "pop", 1.0f, "", "", 200))
            }.toList()

        `when`(localSource.getTracks())
            .thenReturn(Single.just(expected))

        trackRepository.loadTracks()
            .test()
            .assertValue(expected)
            .dispose()
    }

    @Test
    fun `when loading tracks failed, should return error`() {
        val error = Throwable("Something went wrong")

        `when`(localSource.getTracks())
            .thenReturn(Single.error(error))

        trackRepository.loadTracks()
            .test()
            .assertError(error)
            .dispose()
    }
}