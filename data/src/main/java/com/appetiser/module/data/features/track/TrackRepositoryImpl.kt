package com.appetiser.module.data.features.track

import com.appetiser.module.domain.features.track.models.Track
import com.appetiser.module.local.features.track.TrackLocalSource
import com.appetiser.module.network.features.track.TrackRemoteSource
import io.reactivex.Completable
import io.reactivex.Single
import javax.inject.Inject

class TrackRepositoryImpl @Inject constructor(
    private val remoteSource: TrackRemoteSource,
    private val localSource: TrackLocalSource
) : TrackRepository {

    override fun updateTracks(
        term: String,
        country: String,
        media: String
    ): Completable {
        return remoteSource
            .search(term, country, media)
            .doOnSuccess { localSource.saveTracks(it) }
            .ignoreElement()
    }

    override fun loadTracks(): Single<List<Track>> {
        return localSource
            .getTracks()
    }
}
