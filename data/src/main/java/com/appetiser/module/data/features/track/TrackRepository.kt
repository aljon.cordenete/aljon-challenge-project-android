package com.appetiser.module.data.features.track

import com.appetiser.module.domain.features.track.models.Track
import io.reactivex.Completable
import io.reactivex.Single

interface TrackRepository {

    fun updateTracks(term: String, country: String, media: String): Completable

    fun loadTracks(): Single<List<Track>>
}
