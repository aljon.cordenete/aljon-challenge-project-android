package com.appetiser.aljonchallengeproject.features.tracks

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import com.appetiser.aljonchallengeproject.R
import com.appetiser.aljonchallengeproject.base.BaseViewModelActivity
import com.appetiser.aljonchallengeproject.databinding.ActivityTracksBinding
import com.appetiser.aljonchallengeproject.features.trackdetail.TrackDetailActivity
import com.appetiser.aljonchallengeproject.features.tracks.adapter.TracksAdapter
import com.appetiser.module.common.gone
import com.appetiser.module.common.toast
import com.appetiser.module.common.visible
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber

class TracksActivity : BaseViewModelActivity<ActivityTracksBinding, TracksViewModel>() {
    companion object {
        fun openActivity(context: Context) {
            context.startActivity(
                Intent(
                    context,
                    TracksActivity::class.java
                )
            )
        }
    }

    private val tracksAdapter by lazy {
        TracksAdapter(listOf())
    }

    override fun getLayoutId(): Int = R.layout.activity_tracks

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setUpToolbar()
        setUpViews()
        setUpVmObservers()
    }

    private fun setUpToolbar() {
        setToolbarTitle(getString(R.string.tracks_toolbar_title))
    }

    private fun setUpViews() {
        tracksAdapter
            .itemClickListener
            .subscribe { track ->
                TrackDetailActivity.openActivity(this, track)
            }
            .apply { disposables.add(this) }

        binding
            .trackList
            .apply {
                addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
                adapter = tracksAdapter
            }
    }

    private fun setUpVmObservers() {
        viewModel
            .state
            .observeOn(scheduler.ui())
            .subscribeBy(
                onNext = { state ->
                    handleState(state)
                },
                onError = {
                    Timber.e(it)
                }
            )
            .apply { disposables.add(this) }

        viewModel
            .tracks
            .observe(this, Observer { tracks ->
                tracksAdapter.updateList(tracks)
            })
    }

    private fun handleState(state: TracksState) {
        when (state) {
            TracksState.LoadTracks -> {
                viewModel.loadTracks()
            }

            TracksState.ShowFetchError -> {
                toast(getString(R.string.error_message_network))
                viewModel.loadTracks()
            }

            TracksState.ShowLoading -> {
                binding.loading.visible()
            }

            TracksState.HideLoading -> {
                binding.loading.gone()
            }
        }
    }
}
