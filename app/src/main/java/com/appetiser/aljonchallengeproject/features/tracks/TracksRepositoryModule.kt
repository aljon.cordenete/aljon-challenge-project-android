package com.appetiser.aljonchallengeproject.features.tracks

import com.appetiser.aljonchallengeproject.di.scopes.ActivityScope
import com.appetiser.module.data.features.track.TrackRepository
import com.appetiser.module.data.features.track.TrackRepositoryImpl
import com.appetiser.module.local.AppDatabase
import com.appetiser.module.local.features.track.TrackLocalSource
import com.appetiser.module.local.features.track.TrackLocalSourceImpl
import com.appetiser.module.network.BaseplateApiServices
import com.appetiser.module.network.features.track.TrackRemoteSource
import com.appetiser.module.network.features.track.TrackRemoteSourceImpl
import dagger.Module
import dagger.Provides

@Module
class TracksRepositoryModule {

    @Provides
    fun providesTrackLocalSource(
        database: AppDatabase
    ): TrackLocalSource = TrackLocalSourceImpl(database.trackDao())

    @Provides
    fun providesTracksRemoteSource(
        baseplateApiServices: BaseplateApiServices
    ): TrackRemoteSource = TrackRemoteSourceImpl(baseplateApiServices)

    @ActivityScope
    @Provides
    fun providesTrackRepository(
        trackRemoteSource: TrackRemoteSource,
        trackLocalSource: TrackLocalSource
    ): TrackRepository = TrackRepositoryImpl(trackRemoteSource, trackLocalSource)
}
