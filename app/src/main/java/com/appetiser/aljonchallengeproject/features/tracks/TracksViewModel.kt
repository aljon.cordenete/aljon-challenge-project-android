package com.appetiser.aljonchallengeproject.features.tracks

import android.os.Bundle
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.appetiser.aljonchallengeproject.base.BaseViewModel
import com.appetiser.module.data.features.track.TrackRepository
import io.reactivex.Observable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

class TracksViewModel @Inject constructor(
    private val trackRepository: TrackRepository
) : BaseViewModel() {

    private val _state by lazy {
        PublishSubject.create<TracksState>()
    }

    val state: Observable<TracksState> = _state

    private val _tracks by lazy {
        MutableLiveData<List<DisplayableTrack>>()
    }

    val tracks: LiveData<List<DisplayableTrack>> = _tracks

    override fun isFirstTimeUiCreate(bundle: Bundle?) {
        updateTracks()
    }

    fun updateTracks(
        term: String = "star",
        country: String = "au",
        media: String = "movie"
    ) {
        trackRepository
            .updateTracks(term, country, media)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .doOnSubscribe {
                _state.onNext(TracksState.ShowLoading)
            }
            .doOnError {
                _state.onNext(TracksState.HideLoading)
            }
            .doOnComplete {
                _state.onNext(TracksState.HideLoading)
            }
            .subscribeBy(
                onComplete = {
                    _state.onNext(TracksState.LoadTracks)
                },
                onError = {
                    _state.onNext(TracksState.ShowFetchError)
                }
            )
            .apply { disposables.add(this) }
    }

    fun loadTracks() {
        trackRepository
            .loadTracks()
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribeBy { list ->
                _tracks.value = list.map { DisplayableTrack.fromDomain(it) }
            }
            .apply { disposables.add(this) }
    }
}
