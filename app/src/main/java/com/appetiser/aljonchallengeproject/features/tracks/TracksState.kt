package com.appetiser.aljonchallengeproject.features.tracks

sealed class TracksState {

    object LoadTracks : TracksState()

    object ShowLoading : TracksState()

    object HideLoading : TracksState()

    object ShowFetchError : TracksState()
}
