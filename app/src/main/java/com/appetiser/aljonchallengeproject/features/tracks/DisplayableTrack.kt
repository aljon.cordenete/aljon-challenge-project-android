package com.appetiser.aljonchallengeproject.features.tracks

import android.os.Parcelable
import com.appetiser.module.domain.features.track.models.Track
import kotlinx.android.parcel.Parcelize
import java.text.DecimalFormat

@Parcelize
data class DisplayableTrack(
    val trackId: Long,
    val trackName: String,
    val shortDescription: String?,
    val longDescription: String?,
    val genre: String,
    val priceString: String,
    val coverImageUrl: String,
    val itemImageUrl: String,
    val previewUrl: String,
    val durationString: String
) : Parcelable {
    companion object {
        fun fromDomain(
            track: Track
        ): DisplayableTrack {
            return with(track) {

                DisplayableTrack(
                    trackId = trackId,
                    trackName = trackName,
                    shortDescription = shortDescription,
                    longDescription = longDescription,
                    genre = genre,
                    priceString = "AU$ " + DecimalFormat("#,###.##").format(price),
                    coverImageUrl = artwork?.replace("100x100", "200x200"),
                    itemImageUrl = artwork,
                    previewUrl = previewUrl,
                    durationString = (duration / 60000).toString()
                )
            }
        }
    }
}
