package com.appetiser.aljonchallengeproject.features.auth.landing

import android.os.Bundle
import com.appetiser.aljonchallengeproject.base.BaseViewModel
import com.appetiser.module.data.features.auth.AuthRepository
import com.google.gson.Gson
import io.reactivex.Observable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.PublishSubject
import timber.log.Timber
import javax.inject.Inject

class LandingViewModel @Inject constructor(
    private val authRepository: AuthRepository,
    private val gson: Gson
) : BaseViewModel() {

    companion object {
        const val FACEBOOK = "facebook"
        const val GOOGLE = "google"
    }

    private val _state by lazy {
        PublishSubject.create<LandingState>()
    }

    val state: Observable<LandingState> = _state

    override fun isFirstTimeUiCreate(bundle: Bundle?) {
        authRepository
            .getUserSession()
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribeBy(
                onSuccess = { userSession ->
                    if (userSession.id.isNullOrEmpty() || !userSession.verified)
                        return@subscribeBy

                    // TODO temporary fix if lastname and first name is null, you need to login/register again
                    if (userSession.lastName.isNullOrEmpty() || userSession.firstName.isNullOrEmpty()) {
                        return@subscribeBy
                    }

                    // User is logged in.
                    _state
                        .onNext(
                            LandingState.UserIsLoggedIn
                        )
                },
                onError = {
                    Timber.e(it)
                }
            )
            .apply { disposables.add(this) }
    }

    fun onFacebookLogin(facebookAccessToken: String) {
        socialLogin(facebookAccessToken, FACEBOOK)
    }

    fun onGoogleSignIn(googleAccessToken: String) {
        socialLogin(googleAccessToken, GOOGLE)
    }

    private fun socialLogin(
        accessToken: String,
        accessTokenProvider: String
    ) {
        authRepository
            .socialLogin(accessToken, accessTokenProvider)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .doOnSubscribe {
                _state.onNext(LandingState.ShowLoading(accessTokenProvider))
            }
            .doOnSuccess {
                _state.onNext(LandingState.HideLoading)
            }
            .doOnError {
                _state.onNext(LandingState.HideLoading)
            }
            .subscribeBy(
                onSuccess = { result ->
                    Timber.d("onFacebookLogin ${gson.toJson(result)}")
                    _state.onNext(LandingState.LoginSuccess)
                },
                onError = {
                    Timber.e(it)
                    _state.onNext(LandingState.Error(it))
                }
            )
            .apply { disposables.add(this) }
    }
}
