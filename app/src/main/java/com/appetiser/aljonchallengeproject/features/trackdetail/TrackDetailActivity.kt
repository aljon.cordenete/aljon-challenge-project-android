package com.appetiser.aljonchallengeproject.features.trackdetail

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.appetiser.aljonchallengeproject.R
import com.appetiser.aljonchallengeproject.base.BaseActivity
import com.appetiser.aljonchallengeproject.databinding.ActivityTrackDetailBinding
import com.appetiser.aljonchallengeproject.features.tracks.DisplayableTrack
import com.appetiser.module.common.gone
import com.appetiser.module.common.toast

class TrackDetailActivity : BaseActivity<ActivityTrackDetailBinding>() {
    companion object {
        const val KEY_TRACK_OBJECT = "TRACK_PARCELABLE"
        fun openActivity(context: Context, track: DisplayableTrack) {
            val intent = Intent(context, TrackDetailActivity::class.java)
            intent.putExtra(KEY_TRACK_OBJECT, track)
            context.startActivity(intent)
        }
    }

    private val track by lazy {
        intent.getParcelableExtra(KEY_TRACK_OBJECT) as DisplayableTrack
    }

    override fun getLayoutId(): Int = R.layout.activity_track_detail

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupToolbar()
        setUpViews()
    }

    override fun canBack(): Boolean {
        return true
    }

    private fun setupToolbar() {
        enableToolbarHomeIndicator()
        setToolbarNoTitle()
        setToolbarHomeIndicatorIcon(R.drawable.ic_arrow_back_white)
    }

    private fun setUpViews() {
        binding
            .track = track

        binding.preview
            .apply {
                setVideoPath(track.previewUrl)
                setOnErrorListener { mp, what, extra ->
                    toast(context.getString(R.string.error_message_video))
                    false
                }
                setOnPreparedListener { mediaPlayer ->
                    mediaPlayer.isLooping = true
                    binding.previewLoading.gone()
                }

                start()
            }
    }
}
