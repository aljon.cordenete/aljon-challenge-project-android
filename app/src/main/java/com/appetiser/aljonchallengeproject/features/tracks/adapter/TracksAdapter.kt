package com.appetiser.aljonchallengeproject.features.tracks.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import com.appetiser.aljonchallengeproject.R
import com.appetiser.aljonchallengeproject.base.BaseRecyclerViewAdapter
import com.appetiser.aljonchallengeproject.base.ViewHolder
import com.appetiser.aljonchallengeproject.databinding.ItemTracksBinding
import com.appetiser.aljonchallengeproject.features.tracks.DisplayableTrack

class TracksAdapter(private var list: List<DisplayableTrack>) : BaseRecyclerViewAdapter<ItemTracksBinding, DisplayableTrack>() {

    override fun getItemForPosition(position: Int): DisplayableTrack = list[position]

    override fun getLayoutIdForPosition(position: Int): Int = R.layout.item_tracks

    override fun getItemCount(): Int = list.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder<ItemTracksBinding> {
        return ViewHolder(
            ItemTracksBinding
                .inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
        )
    }

    fun updateList(newList: List<DisplayableTrack>) {
        this.list = newList
        notifyDataSetChanged()
    }
}
