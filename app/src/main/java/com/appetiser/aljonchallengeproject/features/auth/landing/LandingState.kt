package com.appetiser.aljonchallengeproject.features.auth.landing

sealed class LandingState {
    object UserIsLoggedIn : LandingState()

    class ShowLoading(val loginType: String) : LandingState()

    object NoUserFirstAndLastName : LandingState()

    object HideLoading : LandingState()

    object LoginSuccess : LandingState()

    class Error(val throwable: Throwable) : LandingState()
}
