package com.appetiser.aljonchallengeproject.features.auth.forgotpassword.verification

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.method.LinkMovementMethod
import android.view.inputmethod.EditorInfo
import androidx.core.content.ContextCompat
import com.appetiser.auth_modern.databinding.ActivityEmailVerificationCodeBinding
import com.appetiser.aljonchallengeproject.R
import com.appetiser.aljonchallengeproject.base.BaseViewModelActivity
import com.appetiser.aljonchallengeproject.ext.enableWithAplhaWhen
import com.appetiser.aljonchallengeproject.features.auth.forgotpassword.newpassword.NewPasswordActivity
import com.appetiser.module.common.gone
import com.appetiser.module.common.ninjaTap
import com.appetiser.module.common.spannableString
import com.appetiser.module.common.toast
import com.appetiser.module.network.base.response.error.ResponseError
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber

class ForgotPasswordVerificationActivity : BaseViewModelActivity<ActivityEmailVerificationCodeBinding, ForgotPasswordVerificationViewModel>() {

    companion object {

        fun openActivity(context: Context, email: String) {
            val intent = Intent(context, ForgotPasswordVerificationActivity::class.java)
            intent.putExtra(KEY_EMAIL, email)
            context.startActivity(intent)
        }

        const val KEY_EMAIL = "email"
    }

    override fun getLayoutId(): Int = R.layout.activity_email_verification_code

    override fun canBack(): Boolean {
        return true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setupViews()
        setupToolbar()
        setupViewModels()
    }

    private fun setupToolbar() {
        enableToolbarHomeIndicator()
        setToolbarNoTitle()
    }

    private fun setupViews() {
        binding.indicatorContainer.gone()

        binding.inputCode.apply {
            setOnEditorActionListener { _, actionId, _ ->
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    if (binding.btnContinue.isEnabled) {
                        viewModel.sendToken(binding.inputCode.text.toString())
                    }
                    true
                } else {
                    false
                }
            }
        }

        binding.btnContinue.enableWithAplhaWhen(binding.inputCode) { it.isNotEmpty() && it.length >= 5 }

        disposables.add(binding.btnContinue.ninjaTap {
            viewModel.sendToken(binding.inputCode.text.toString())
        })

        disposables.add(binding.noCode.ninjaTap {
            viewModel.resendCode()
        })
    }

    private fun setupViewModels() {
        viewModel.state
            .observeOn(scheduler.ui())
            .subscribeBy(
                onNext = { state ->
                    when (state) {
                        is ForgotPasswordVerificationState.GetEmail -> {

                            binding.onboardingWelcomeBack.movementMethod = LinkMovementMethod.getInstance()
                            binding.onboardingWelcomeBack.text = getString(R.string.verification_code_description, state.email)
                                .spannableString(
                                    this,
                                    null,
                                    R.font.inter_medium,
                                    ContextCompat.getColor(this, R.color.colorPrimaryDark),
                                    state.email,
                                    clickable = {
                                    }
                                )
                        }

                        is ForgotPasswordVerificationState.ResendTokenSuccess -> {
                            toast("New code sent!")
                        }

                        is ForgotPasswordVerificationState.ForgotPasswordSuccess -> {
                            NewPasswordActivity.openActivity(this@ForgotPasswordVerificationActivity, state.email, state.token)
                            finish()
                        }

                        is ForgotPasswordVerificationState.Error -> {
                            ResponseError.getError(state.throwable,
                                ResponseError.ErrorCallback(httpExceptionCallback = {
                                    toast(it)
                                }))
                        }

                        is ForgotPasswordVerificationState.ShowProgressLoading -> {
                            toast("Sending request")
                        }

                        is ForgotPasswordVerificationState.HideProgressLoading -> {
                        }
                    }
                },
                onError = {
                    Timber.e(it)
                }
            )
            .apply { disposables.add(this) }
    }
}
