package com.appetiser.aljonchallengeproject.di

import com.appetiser.aljonchallengeproject.utils.schedulers.BaseSchedulerProvider
import com.appetiser.aljonchallengeproject.utils.schedulers.SchedulerProvider
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class SchedulerModule {

    @Provides
    @Singleton
    fun providesSchedulerSource(): BaseSchedulerProvider =
            SchedulerProvider.getInstance()
}
