package com.appetiser.aljonchallengeproject.di

/**
 * Marks an activity / fragment injectable.
 */
interface Injectable
