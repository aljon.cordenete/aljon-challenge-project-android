package com.appetiser.aljonchallengeproject.di.builders

import com.appetiser.aljonchallengeproject.di.scopes.ActivityScope
import com.appetiser.aljonchallengeproject.features.auth.AuthRepositoryModule
import com.appetiser.aljonchallengeproject.features.auth.emailcheck.EmailCheckActivity
import com.appetiser.aljonchallengeproject.features.auth.forgotpassword.ForgotPasswordActivity
import com.appetiser.aljonchallengeproject.features.auth.forgotpassword.newpassword.NewPasswordActivity
import com.appetiser.aljonchallengeproject.features.auth.forgotpassword.verification.ForgotPasswordVerificationActivity
import com.appetiser.aljonchallengeproject.features.auth.landing.LandingActivity
import com.appetiser.aljonchallengeproject.features.auth.login.LoginActivity
import com.appetiser.aljonchallengeproject.features.auth.register.RegisterActivity
import com.appetiser.aljonchallengeproject.features.auth.register.details.UserDetailsActivity
import com.appetiser.aljonchallengeproject.features.auth.register.verification.RegisterVerificationActivity
import com.appetiser.aljonchallengeproject.features.main.MainActivity
import com.appetiser.aljonchallengeproject.features.trackdetail.TrackDetailActivity
import com.appetiser.aljonchallengeproject.features.tracks.TracksActivity
import com.appetiser.aljonchallengeproject.features.tracks.TracksRepositoryModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilder {

    @ActivityScope
    @ContributesAndroidInjector(modules = [(AuthRepositoryModule::class)])
    abstract fun contributeEmailCheckActivity(): EmailCheckActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [(AuthRepositoryModule::class)])
    abstract fun contributeLoginActivity(): LoginActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [(AuthRepositoryModule::class)])
    abstract fun contributeForgotPasswordActivity(): ForgotPasswordActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [(AuthRepositoryModule::class)])
    abstract fun contributeRegisterVerificationActivity(): RegisterVerificationActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [(AuthRepositoryModule::class)])
    abstract fun contributeRegisterActivity(): RegisterActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [(AuthRepositoryModule::class)])
    abstract fun contributeForgotPasswordVerificationActivity(): ForgotPasswordVerificationActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [(AuthRepositoryModule::class)])
    abstract fun contributeNewPasswordActivity(): NewPasswordActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [(AuthRepositoryModule::class)])
    abstract fun contributeUserDetailsActivity(): UserDetailsActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [(AuthRepositoryModule::class)])
    abstract fun contributeMainActivity(): MainActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [(AuthRepositoryModule::class)])
    abstract fun contributeLandingActivity(): LandingActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [(TracksRepositoryModule::class)])
    abstract fun contributeTracksActivity(): TracksActivity

    @ActivityScope
    @ContributesAndroidInjector()
    abstract fun contributeTrackDetailActivity(): TrackDetailActivity
}
