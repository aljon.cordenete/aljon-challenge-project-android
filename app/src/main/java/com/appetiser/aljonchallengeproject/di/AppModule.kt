package com.appetiser.aljonchallengeproject.di

import android.content.Context
import com.appetiser.aljonchallengeproject.AljonchallengeprojectApplication
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
abstract class AppModule {

    @Singleton
    @Binds
    abstract fun providesApplicationContext(app: AljonchallengeprojectApplication): Context
}
