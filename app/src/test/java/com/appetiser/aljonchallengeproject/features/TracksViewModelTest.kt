package com.appetiser.aljonchallengeproject.features

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.appetiser.aljonchallengeproject.core.TestSchedulerProvider
import com.appetiser.aljonchallengeproject.features.auth.login.LoginState
import com.appetiser.aljonchallengeproject.features.tracks.DisplayableTrack
import com.appetiser.aljonchallengeproject.features.tracks.TracksState
import com.appetiser.aljonchallengeproject.features.tracks.TracksViewModel
import com.appetiser.module.data.features.auth.AuthRepository
import com.appetiser.module.data.features.track.TrackRepository
import com.appetiser.module.domain.features.track.models.Track
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.observers.TestObserver
import io.reactivex.schedulers.TestScheduler
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.ArgumentCaptor
import org.mockito.ArgumentMatchers.anyString
import org.mockito.Mockito
import org.mockito.Mockito.`when`

class TracksViewModelTest {

    @get: Rule
    val rule = InstantTaskExecutorRule()

    //class under test
    private lateinit var tracksViewModel: TracksViewModel

    private val testScheduler = TestScheduler()
    private val schedulers = TestSchedulerProvider(testScheduler)

    private val trackRepository = Mockito.mock(TrackRepository::class.java)
    private val observer = Mockito.mock(TestObserver::class.java) as TestObserver<TracksState>

    @Before
    fun setUp() {
        tracksViewModel = TracksViewModel(trackRepository)
        tracksViewModel.schedulers = schedulers
        tracksViewModel.state.subscribe(observer)
    }

    @Test
    fun `if update Tracks is successful, expect state LoadTracks`() {
        val expected = TracksState.LoadTracks

        `when`(trackRepository.updateTracks(anyString(), anyString(), anyString())).thenReturn(Completable.complete())

        tracksViewModel.updateTracks()
        testScheduler.triggerActions()

        ArgumentCaptor.forClass(TracksState.LoadTracks::class.java).run {
            Mockito.verify(observer, Mockito.times(3)).onNext(capture())
            Assert.assertEquals(expected, value)
        }
    }

    @Test
    fun `if update Tracks failed, expect state ShowFetchError`() {
        val expected = TracksState.ShowFetchError

        val error = Throwable("Something went wrong")

        `when`(trackRepository.updateTracks(anyString(), anyString(), anyString()))
            .thenReturn(Completable.error(error))

        tracksViewModel.updateTracks()
        testScheduler.triggerActions()

        ArgumentCaptor.forClass(TracksState::class.java).run {
            Mockito.verify(observer, Mockito.times(3)).onNext(capture())
            Assert.assertEquals(expected, value)
        }
    }

    @Test
    fun `if load Tracks is successful, should return a list of tracks`() {
        val expected = arrayListOf<Track>()
            .apply {
                add(Track(1, "track1", "shortDesc", "longDesc", "pop", 1.0f, "", "", 200))
                add(Track(2, "track2", "shortDesc", "longDesc", "pop", 1.0f, "", "", 200))
            }.toList()

        `when`(trackRepository.loadTracks()).thenReturn(Single.just(expected))

        //maps the expected domain list to displayable list
        val expectedDisplayable = expected.map { DisplayableTrack.fromDomain(it) }

        tracksViewModel.loadTracks()
        testScheduler.triggerActions()

        Assert.assertEquals(expectedDisplayable, tracksViewModel.tracks.value)
    }
}