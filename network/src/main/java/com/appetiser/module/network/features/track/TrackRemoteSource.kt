package com.appetiser.module.network.features.track

import com.appetiser.module.domain.features.track.models.Track
import io.reactivex.Single

interface TrackRemoteSource {

    fun search(term: String, country: String, media: String): Single<List<Track>>
}
