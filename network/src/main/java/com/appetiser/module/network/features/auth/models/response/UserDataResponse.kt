package com.appetiser.module.network.features.auth.models.response

import com.appetiser.module.network.features.auth.models.UserSessionDTO
import com.google.gson.annotations.SerializedName

open class UserDataResponse(
    val user: UserSessionDTO,
    @field:SerializedName("access_token")
    val accessToken: String = "",
    @field:SerializedName("token_type")
    val tokenType: String? = "",
    @field:SerializedName("expires_in")
    val expiresIn: String? = ""
)
