package com.appetiser.module.network.features.track.models

import com.appetiser.module.domain.features.track.models.Track
import com.google.gson.annotations.SerializedName

data class TrackDTO(
    @field:SerializedName("trackId")
    val trackId: Long,
    @field:SerializedName("trackName")
    val trackName: String? = null,
    @SerializedName("shortDescription")
    val shortDescription: String? = null,
    @SerializedName("longDescription")
    val longDescription: String? = null,
    @field:SerializedName("primaryGenreName")
    val genre: String? = null,
    @field:SerializedName("trackPrice")
    val price: Float,
    @field:SerializedName("artworkUrl100")
    val artwork: String? = null,
    @field:SerializedName("previewUrl")
    val previewUrl: String? = null,
    @field:SerializedName("trackTimeMillis")
    val durationMillis: Int
) {
    companion object {
        fun toDomain(trackDTO: TrackDTO): Track {
            return with(trackDTO) {
                Track(
                    trackId = trackId,
                    trackName = trackName.orEmpty(),
                    shortDescription = shortDescription.orEmpty(),
                    longDescription = longDescription.orEmpty(),
                    genre = genre.orEmpty(),
                    price = price,
                    artwork = artwork.orEmpty(),
                    previewUrl = previewUrl.orEmpty(),
                    duration = durationMillis
                )
            }
        }
    }
}
