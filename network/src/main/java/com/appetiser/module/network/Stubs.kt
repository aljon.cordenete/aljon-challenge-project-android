package com.appetiser.module.network

import com.appetiser.module.domain.features.auth.models.CountryCode

object Stubs {
    fun countries(): List<CountryCode> {
        return mutableListOf<CountryCode>()
            .apply {
                add(CountryCode(1, "PH", "63", "/images/flags/PH.png"))
                add(CountryCode(2, "AU", "61", "/images/flags/AU.png"))
            }
    }
}
