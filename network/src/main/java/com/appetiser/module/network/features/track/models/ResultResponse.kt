package com.appetiser.module.network.features.track.models

import com.google.gson.annotations.SerializedName

data class ResultResponse<T>(
    @SerializedName("results")
    val results: List<T>,
    @SerializedName("resultCount")
    val resultCount: Int
)
