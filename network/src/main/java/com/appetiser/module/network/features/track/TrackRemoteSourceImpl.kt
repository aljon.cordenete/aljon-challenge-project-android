package com.appetiser.module.network.features.track

import com.appetiser.module.domain.features.track.models.Track
import com.appetiser.module.network.BaseplateApiServices
import com.appetiser.module.network.features.track.models.TrackDTO
import io.reactivex.Single
import javax.inject.Inject

class TrackRemoteSourceImpl @Inject constructor(
    private val baseplateApiServices: BaseplateApiServices
) : TrackRemoteSource {

    override fun search(
        term: String,
        country: String,
        media: String
    ): Single<List<Track>> {
        return baseplateApiServices
            .search(term, country, media)
            .map { response ->
                response
                    .results
                    .map { TrackDTO.toDomain(it) }
            }
    }
}
