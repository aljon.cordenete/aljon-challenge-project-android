package com.appetiser.module.local.features.track

import com.appetiser.module.domain.features.track.models.Track
import com.appetiser.module.local.features.track.dao.TrackDao
import com.appetiser.module.local.features.track.models.DBTrack
import io.reactivex.Single
import org.junit.Before

import org.junit.Assert.*
import org.junit.Test
import org.mockito.ArgumentMatchers.anyList
import org.mockito.Mockito.*

class TrackLocalSourceImplTest {

    //class under test
    private lateinit var trackLocalSource: TrackLocalSourceImpl

    private val trackDao = mock(TrackDao::class.java)

    @Before
    fun setUp() {
        trackLocalSource = TrackLocalSourceImpl(trackDao)
    }

    @Test
    fun `when saving list of tracks, should return list of IDs`() {
        val expected = mutableListOf<Track>()
            .apply {
                add(Track(1, "track1", "shortDesc", "longDesc", "pop", 1.0f, "", "", 200))
                add(Track(2, "track2", "shortDesc", "longDesc", "pop", 1.0f, "", "", 200))
            }
        //get ids from expected list...
        val expectedIDs = expected.map { it.trackId }

        `when`(trackDao.insert(anyList())).thenReturn(expectedIDs)

        val insertedIDs = trackLocalSource.saveTracks(expected)

        assertEquals(expectedIDs, insertedIDs)

        verify(trackDao, times(1)).insert(anyList())
    }

    @Test
    fun `when getting all tracks, should return list of track`() {
        val expected = mutableListOf<Track>()
            .apply {
                add(Track(1, "track1", "shortDesc", "longDesc", "pop", 1.0f, "", "", 200))
                add(Track(2, "track2", "shortDesc", "longDesc", "pop", 1.0f, "", "", 200))
            }

        `when`(trackDao.getAll())
            .thenReturn(
                Single.just(
                    expected.map { DBTrack.fromDomain(it) }
                )
            )

        trackLocalSource.getTracks()
            .test()
            .assertValue(expected)
            .dispose()
    }

    @Test
    fun `when getting all tracks failed, should return error`() {
        val error = Throwable("Something went wrong")

        `when`(trackDao.getAll())
            .thenReturn(Single.error(error))

        trackLocalSource.getTracks()
            .test()
            .assertError(error)
            .dispose()
    }
}