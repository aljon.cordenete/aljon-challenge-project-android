package com.appetiser.module.local.features.auth

import androidx.room.EmptyResultSetException
import androidx.room.Transaction
import com.appetiser.module.domain.features.auth.models.AccessToken
import com.appetiser.module.domain.features.auth.models.UserSession
import com.appetiser.module.domain.utils.OnErrorResumeNext
import com.appetiser.module.local.AppDatabase
import com.appetiser.module.local.features.auth.models.DBToken
import com.appetiser.module.local.features.auth.models.DBUserSession
import io.reactivex.Completable
import io.reactivex.Single
import javax.inject.Inject

class AuthLocalSourceImpl @Inject constructor(
    private val database: AppDatabase
) : AuthLocalSource {

    @Transaction
    override fun saveCredentials(user: UserSession): Single<UserSession> {
        return Single.create { emitter ->
            val dbUser = DBUserSession.fromDomain(user)
            database
                .userSessionDao()
                .logoutUser()
            database
                .userSessionDao()
                .insertOrUpdate(dbUser)

            emitter.onSuccess(user)
        }
    }

    override fun saveToken(token: String): Single<AccessToken> {
        return Single
            .create { emitter ->
                database
                    .tokenDao()
                    .insert(
                        DBToken(token = token)
                    )

                emitter.onSuccess(AccessToken(token))
            }
    }

    override fun getUserSession(): Single<UserSession> {
        return database
            .userSessionDao()
            .getUserInfo()
            .compose(
                OnErrorResumeNext<DBUserSession, EmptyResultSetException>(
                    DBUserSession.empty(),
                    EmptyResultSetException::class.java
                )
            )
            .map {
                DBUserSession.toDomain(it)
            }
    }

    override fun getAccessToken(): Single<AccessToken> {
        return database.tokenDao().getToken()
            .map {
                DBToken.toDomain(it)
            }
    }

    @Transaction
    override fun logout(): Completable {
        return Completable.create {
            database.userSessionDao().logoutUser()
            database.tokenDao().logoutToken()
            it.onComplete()
        }
    }
}
