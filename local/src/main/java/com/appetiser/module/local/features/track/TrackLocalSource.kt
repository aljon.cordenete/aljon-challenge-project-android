package com.appetiser.module.local.features.track

import com.appetiser.module.domain.features.track.models.Track
import io.reactivex.Single

interface TrackLocalSource {

    fun getTracks(): Single<List<Track>>

    fun saveTracks(tracks: List<Track>): List<Long>
}
