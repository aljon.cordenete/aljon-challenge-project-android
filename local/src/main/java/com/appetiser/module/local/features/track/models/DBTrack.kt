package com.appetiser.module.local.features.track.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.appetiser.module.domain.features.track.models.Track

@Entity(tableName = DBTrack.TRACK_TABLE_NAME)
data class DBTrack(
    @PrimaryKey
    @ColumnInfo(name = "track_id")
    val trackId: Long,
    @ColumnInfo(name = "track_name")
    val trackName: String,
    @ColumnInfo(name = "short_description")
    val shortDescription: String?,
    @ColumnInfo(name = "long_description")
    val longDescription: String?,
    @ColumnInfo(name = "genre")
    val genre: String,
    @ColumnInfo(name = "price")
    val price: Float,
    @ColumnInfo(name = "artwork")
    val artwork: String,
    @ColumnInfo(name = "preview_url")
    val previewUrl: String,
    @ColumnInfo(name = "duration")
    val duration: Int

) {
    companion object {
        const val TRACK_TABLE_NAME = "track"

        fun fromDomain(track: Track): DBTrack {
            return with(track) {
                DBTrack(
                    trackId = trackId,
                    trackName = trackName,
                    shortDescription = shortDescription,
                    longDescription = longDescription,
                    genre = genre,
                    price = price,
                    artwork = artwork,
                    previewUrl = previewUrl,
                    duration = duration
                )
            }
        }

        fun toDomain(trackDB: DBTrack): Track {
            return with(trackDB) {
                Track(
                    trackId = trackId,
                    trackName = trackName,
                    shortDescription = shortDescription,
                    longDescription = longDescription,
                    genre = genre,
                    price = price,
                    artwork = artwork,
                    previewUrl = previewUrl,
                    duration = duration
                )
            }
        }
    }
}
