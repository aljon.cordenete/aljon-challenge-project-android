package com.appetiser.module.local.features.track

import com.appetiser.module.domain.features.track.models.Track
import com.appetiser.module.local.features.track.dao.TrackDao
import com.appetiser.module.local.features.track.models.DBTrack
import io.reactivex.Single
import javax.inject.Inject

class TrackLocalSourceImpl @Inject constructor(
    private val trackDao: TrackDao
) : TrackLocalSource {

    override fun getTracks(): Single<List<Track>> {
        return trackDao
            .getAll()
            .map { tracksDB ->
                tracksDB
                    .map { DBTrack.toDomain(it) }
            }
    }

    override fun saveTracks(tracks: List<Track>): List<Long> {
        return trackDao
            .insert(
                tracks.map {
                    DBTrack.fromDomain(it)
                }.toMutableList()
            )
    }
}
