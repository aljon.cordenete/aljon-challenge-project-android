package com.appetiser.module.local.features.track.dao

import androidx.room.Dao
import androidx.room.Query
import com.appetiser.module.local.base.BaseDao
import com.appetiser.module.local.features.track.models.DBTrack
import io.reactivex.Single

@Dao
abstract class TrackDao : BaseDao<DBTrack> {

    @Query("Select * from track")
    abstract fun getAll(): Single<List<DBTrack>>
}
