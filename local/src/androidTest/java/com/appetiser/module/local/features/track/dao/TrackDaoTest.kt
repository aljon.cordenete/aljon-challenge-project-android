package com.appetiser.module.local.features.track.dao

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.MediumTest
import androidx.test.filters.SmallTest
import com.appetiser.module.domain.features.track.models.Track
import com.appetiser.module.local.AppDatabase
import com.appetiser.module.local.features.track.TrackLocalSourceImpl
import com.appetiser.module.local.features.track.models.DBTrack
import org.junit.After
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@SmallTest
class TrackDaoTest {

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    //class under test
    private lateinit var trackDao: TrackDao

    private lateinit var database: AppDatabase

    @Before
    fun setUp() {
        // using an in-memory database for testing, since it doesn't survive killing the process
        database = Room.inMemoryDatabaseBuilder(
            ApplicationProvider.getApplicationContext(),
            AppDatabase::class.java
        )
            .allowMainThreadQueries()
            .build()

        trackDao = database.trackDao()
    }

    @After
    fun tearDown() {
        database.close()
    }

    @Test
    fun insertingTracksAndGettingAll() {
        val expected = mutableListOf<DBTrack>()
            .apply {
                add(DBTrack(1, "track1", "shortDesc", "longDesc", "pop", 1.0f, "", "", 200))
                add(DBTrack(2, "track2", "shortDesc", "longDesc", "pop", 1.0f, "", "", 200))
            }

        //when inserting list of tracks in database
        trackDao.insert(expected)

        //then getting all tracks
        trackDao.getAll()
            .test()
            .assertValue(expected)
            .dispose()
    }
}